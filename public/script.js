function click1() {
    let price = document.getElementsByName("price");
    let count = document.getElementsByName("count");
    let result = document.getElementById("result");
    let toPrice = price[0].value;
    let toCount = count[0].value;
    if(toPrice.match(/^\d+$/) !== null && toCount.match(/^\d+$/) !== null ){
     result.innerHTML = parseInt(toPrice) * parseInt(toCount); 
    }
    else {
      result.innerHTML="Incorrect input!";
    }
    return false;
  }
  function onClick() {
    alert("Done!");
  }
  window.addEventListener("DOMContentLoaded", function (event) {
    console.log("DOM fully loaded and parsed");
    let b = document.getElementById("calculate");
    b.addEventListener("click", onClick);
  });



